package co.com.emcali.hallazgoscontraloria.constants;

/**
 * @author salek
 */
public class HallazgosContraloriaPortletKeys {

	public static final String HALLAZGOSCONTRALORIAREGISTRO = "co_com_emcali_hallazgoscontraloria_HallazgosContraloriaRegistroPortlet";
	public static final String HALLAZGOSCONTRALORIASEGUIMIENTO = "co_com_emcali_hallazgoscontraloria_HallazgosContraloriaSeguimientoPortlet";
	public static final String HALLAZGOSCONTRALORIACONSULTA = "co_com_emcali_hallazgoscontraloria_HallazgosContraloriaConsultaPortlet";
	public static final String HALLAZGOSCONTRALORIAVERSEGUIMIENTO = "co_com_emcali_hallazgoscontraloria_HallazgosContraloriaVerSeguimientoPortlet";
	public static final String HALLAZGOSCONTRALORIAREGISTROFUENTE = "co_com_emcali_hallazgoscontraloria_HallazgosContraloriaRegistroFuentePortlet";
	
}