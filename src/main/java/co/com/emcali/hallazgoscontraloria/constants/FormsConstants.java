package co.com.emcali.hallazgoscontraloria.constants;

import com.liferay.portal.kernel.util.PropsUtil;

/**
 * @author salek
 */
public class FormsConstants {

	
	public static final String TEMPLATE_BODY = "com/hallazgos/form/templates/template_body.tmpl";
	
	public static final String FORM_EMAILFROM = "direcciondecontrolinternopm@emcali.com.co";
	public static final String FORM_NAMEFROM = "direcciondecontrolinternopm@emcali.com.co";
	public static final String FORM_SUBJECT = "Se ha generado un nuevo  hallazgo";

	

}
