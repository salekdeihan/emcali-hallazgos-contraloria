package co.com.emcali.hallazgoscontraloria.constants;

/**
 * 
 * @author viventic
 *
 */
public class Constantes {

	public static final String WS_SERVIDOR_PRODUCCION = "http://172.18.1.28:9080/servicios-creg/rest/creg";
	public static final String WS_SERVIDOR_PRUEBAS = "http://172.18.30.114:9081/servicios-creg/rest/creg";
	public static final String WS_SERVIDOR_DEV = "http://ec2-44-200-69-116.compute-1.amazonaws.com:8080/api";

	public static final String WS_CONSUMO_SERVICIOS = WS_SERVIDOR_DEV;

	public static final String URL_AUDIT_FINDBYID = WS_CONSUMO_SERVICIOS + "/audits";
	public static final String URL_AUDIT_ADD = WS_CONSUMO_SERVICIOS + "/audits/add";
	public static final String URL_AUDIT_FINDALL = WS_CONSUMO_SERVICIOS + "/audits/findAll";
	public static final String URL_AUDIT_FINDLAST = WS_CONSUMO_SERVICIOS + "/audits/findLast";
	public static final String URL_AUDIT_EXPORT = WS_CONSUMO_SERVICIOS + "/audits/export";
	
	public static final String URL_DESCARGAR_PLANTILLA = "http://172.18.30.145:3000/Ecoproyect/hallazgos";

	public static final String URL_FINDING_FINDBYID = WS_CONSUMO_SERVICIOS + "/findings";
	public static final String URL_FINDING_FINDALL = WS_CONSUMO_SERVICIOS + "/findings/findAll";
	public static final String URL_FINDING_FINDLAST = WS_CONSUMO_SERVICIOS + "/findings/findLast";
	public static final String URL_FINDING_ADD = WS_CONSUMO_SERVICIOS + "/findings/add";
	public static final String URL_FINDING_FINDBYNUMEROHALLAZGO = WS_CONSUMO_SERVICIOS + "/findings/code";
	public static final String URL_FINDING_EXPORTAR = WS_CONSUMO_SERVICIOS + "/findings/exportar";
	public static final String URL_FINDING_CLOSING = WS_CONSUMO_SERVICIOS + "/findings/closeFindings";
	public static final String URL_FINDING_AMPLIAR = WS_CONSUMO_SERVICIOS + "/findings/clone";

	public static final String URL_FOLLOW_FINDALL = WS_CONSUMO_SERVICIOS + "/follows/findAll";
	public static final String URL_FOLLOW_FINDBYFINDINGID = WS_CONSUMO_SERVICIOS + "/follows/findings";
	public static final String URL_FOLLOW_ADD = WS_CONSUMO_SERVICIOS + "/follows/add";

	public static final String URL_PROGRESO_FINDALL = WS_CONSUMO_SERVICIOS + "/progresos/findAll";
	public static final String URL_PROGRESO_FINDBYFINDINGID = WS_CONSUMO_SERVICIOS + "/progresos/findings";
	public static final String URL_PROGRESO_ADD = WS_CONSUMO_SERVICIOS + "/progresos/add";

	public static final String URL_CONSUMO_FOLLOW_DELETE = WS_CONSUMO_SERVICIOS + "/follows/inactive";

	public static final Integer TIPO_PERSONA_NATURAL = 0;
	public static final Integer TIPO_PERSONA_JURIDICO = 1;
	public static final Integer TIPO_DOCUMENTO_NIT = 0;
	public static final Integer TIPO_DOCUMENTO_CEDULA = 1;

}
