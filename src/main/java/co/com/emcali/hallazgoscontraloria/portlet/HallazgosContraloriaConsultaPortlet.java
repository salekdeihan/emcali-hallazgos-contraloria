package co.com.emcali.hallazgoscontraloria.portlet;

import co.com.emcali.hallazgoscontraloria.constants.HallazgosContraloriaPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author salek
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=EMCALI",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=Hallazgos Contraloria",
		"javax.portlet.init-param.template-path=/jsp/",
		"javax.portlet.init-param.view-template=/jsp/listadoRegistro.jsp",
		"javax.portlet.name=" + HallazgosContraloriaPortletKeys.HALLAZGOSCONTRALORIACONSULTA,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class HallazgosContraloriaConsultaPortlet extends MVCPortlet {
}