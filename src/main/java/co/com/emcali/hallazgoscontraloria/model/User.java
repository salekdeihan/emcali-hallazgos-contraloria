package co.com.emcali.hallazgoscontraloria.model;

import java.io.Serializable;

/**
 * The persistent class for the tbl_users database table.
 * 
 */
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String email;
	private String password;
	private String cargo;
	private String role;
	private String status;
	private User userCreated;
	private User userEdited;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(User userCreated) {
		this.userCreated = userCreated;
	}

	public User getUserEdited() {
		return userEdited;
	}

	public void setUserEdited(User userEdited) {
		this.userEdited = userEdited;
	}

}