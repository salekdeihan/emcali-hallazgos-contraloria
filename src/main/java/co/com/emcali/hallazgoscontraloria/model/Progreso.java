package co.com.emcali.hallazgoscontraloria.model;

import java.io.Serializable;
import java.util.Date;

public class Progreso implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private Finding finding;
	private String descripcion;
	private String observaciones;
	private Date fechaAvance;
	private Float avance;
	private String adjunto;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Finding getFinding() {
		return finding;
	}

	public void setFinding(Finding finding) {
		this.finding = finding;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Date getFechaAvance() {
		return fechaAvance;
	}

	public void setFechaAvance(Date fechaAvance) {
		this.fechaAvance = fechaAvance;
	}

	public Float getAvance() {
		return avance;
	}

	public void setAvance(Float avance) {
		this.avance = avance;
	}

	public String getAdjunto() {
		return adjunto;
	}

	public void setAdjunto(String adjunto) {
		this.adjunto = adjunto;
	}

}