package co.com.emcali.hallazgoscontraloria.model;

import java.io.Serializable;
import java.util.Date;

public class Finding implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private String accion;
	private String codigo;
	private String connotacion;
	private Integer cumplimiento;
	private String descripcion;
	private String direccion;
	private Integer efectividad;
	private String estado;
	private String macroproceso;
	private String meta;
	private String observaciones;
	private String status;
	private Date terminacion;
	private Audit audit;
	private User userCreated;
	private String email;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getConnotacion() {
		return connotacion;
	}

	public void setConnotacion(String connotacion) {
		this.connotacion = connotacion;
	}

	public Integer getCumplimiento() {
		return cumplimiento;
	}

	public void setCumplimiento(Integer cumplimiento) {
		this.cumplimiento = cumplimiento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Integer getEfectividad() {
		return efectividad;
	}

	public void setEfectividad(Integer efectividad) {
		this.efectividad = efectividad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMacroproceso() {
		return macroproceso;
	}

	public void setMacroproceso(String macroproceso) {
		this.macroproceso = macroproceso;
	}

	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTerminacion() {
		return terminacion;
	}

	public void setTerminacion(Date terminacion) {
		this.terminacion = terminacion;
	}

	public Audit getAudit() {
		return audit;
	}

	public void setAudit(Audit audit) {
		this.audit = audit;
	}

	public User getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(User userCreated) {
		this.userCreated = userCreated;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}