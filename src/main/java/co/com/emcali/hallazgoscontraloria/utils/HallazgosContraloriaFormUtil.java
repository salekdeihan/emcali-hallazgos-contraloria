package co.com.emcali.hallazgoscontraloria.utils;

import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.petra.content.ContentUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import co.com.emcali.hallazgoscontraloria.constants.FormsConstants;

public class HallazgosContraloriaFormUtil {
	private static Log _log = LogFactoryUtil.getLog(HallazgosContraloriaFormUtil.class);

	public static boolean sendEmailPersonal(String nameTo, String emailTo, String auditoria, String numeroHallazgo)
			throws AddressException, UnsupportedEncodingException {

		boolean resultSendEmail = true;

		try {

			String subject = FormsConstants.FORM_SUBJECT;
			String emailFrom = FormsConstants.FORM_EMAILFROM;
			String nameFrom = FormsConstants.FORM_NAMEFROM;

			String body = ContentUtil.get(HallazgosContraloriaFormUtil.class.getClassLoader(),
					FormsConstants.TEMPLATE_BODY);
			body = StringUtil.replace(body, new String[] { "[$AUDITORIA$]", "[$NUMEROHALLAZGO$]" },
					new String[] { auditoria, numeroHallazgo });
			
			
			_log.info("nameTo ..." + nameTo);
			_log.info("emailTo ..." + emailTo);
			_log.info("nameFrom ..." + nameFrom);
			_log.info("emailFrom ..." + emailFrom);
			_log.info("subject ..." + subject);

			_log.info("cuerpo ..." + body);

			sendEmail(nameTo, emailTo, nameFrom, emailFrom, subject, body, null);

		} catch (Exception e) {

			_log.error("No se ha podido enviar los datos al email correspondiente: " + e.getMessage());
			resultSendEmail = false;
		}

		return resultSendEmail;

	}

	private static void sendEmail(String nameTo, String mailTo, String nameFrom, String mailFrom, String subject,
			String body, File file) throws AddressException, UnsupportedEncodingException {

		String[] mailtToArray = StringUtil.split(mailTo, ",");
		

		InternetAddress fromAddress = new InternetAddress(mailFrom, nameFrom);
		InternetAddress[] toAddress = new InternetAddress[mailtToArray.length];

		if (nameTo == null || nameTo.isEmpty()) {

			for (int i = 0; i < mailtToArray.length; i++) {
				toAddress[i] = new InternetAddress(mailtToArray[i]);
			}

		} else {
			toAddress[0] = new InternetAddress(mailtToArray[0], nameTo);
		}

		MailMessage mailMessage = new MailMessage();
		mailMessage.setTo(toAddress);
		mailMessage.setFrom(fromAddress);
		mailMessage.setSubject(subject);
		mailMessage.setBody(body);
		mailMessage.setHTMLFormat(true);

		if (Validator.isNotNull(file)) {
			mailMessage.addFileAttachment(file);
		}

		MailServiceUtil.sendEmail(mailMessage);

	}
}
