package co.com.emcali.hallazgoscontraloria.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.Base64;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import co.com.emcali.hallazgoscontraloria.constants.Constantes;
import co.com.emcali.hallazgoscontraloria.model.Audit;
import co.com.emcali.hallazgoscontraloria.model.Finding;
import co.com.emcali.hallazgoscontraloria.model.Follow;
import co.com.emcali.hallazgoscontraloria.model.Progreso;

public class HallazgosContraloriaUtil {
	private static final Log LOGGER = LogFactoryUtil.getLog(HallazgosContraloriaUtil.class);

	public static Audit addAudit(Audit audit) throws Exception {

		StringBuilder sb = new StringBuilder();

		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String json = gson.toJson(audit);
		LOGGER.info("tk json " + json);
		HttpURLConnection conn = null;

		try {
			LOGGER.info(Constantes.URL_AUDIT_ADD);
			URL urlVal = new URL(Constantes.URL_AUDIT_ADD);
			conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);

			OutputStreamWriter dout = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
			dout.write(json);
			dout.flush();
			dout.close();

			int responseCode = conn.getResponseCode();
			LOGGER.info("tk responseCode " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String jsonResponse = sb.toString();
			LOGGER.info("tk jsonResponse " + jsonResponse);

			Type findingType = new TypeToken<Audit>() {
			}.getType();

			Audit auditResponse = gson.fromJson(jsonResponse, findingType);
			return auditResponse;

		} catch (IOException exception) {
			throw new Exception(exception);
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

	public static Finding addRegistro(Finding findigs) throws Exception {

		StringBuilder sb = new StringBuilder();

		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String json = gson.toJson(findigs);
		LOGGER.info("tk json " + json);

		HttpURLConnection conn = null;

		try {

			LOGGER.info(Constantes.URL_FINDING_ADD);
			URL urlVal = new URL(Constantes.URL_FINDING_ADD);
			conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);

			OutputStreamWriter dout = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
			dout.write(json);
			dout.flush();
			dout.close();

			int responseCode = conn.getResponseCode();
			LOGGER.info("tk responseCode " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String jsonResponse = sb.toString();
			LOGGER.info("tk jsonResponse " + jsonResponse);

			Type findingType = new TypeToken<Finding>() {
			}.getType();

			Finding finding = gson.fromJson(jsonResponse, findingType);
			return finding;

		} catch (IOException exception) {
			throw new Exception(exception);
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

	public static Finding closingRegistro(Finding finding) throws Exception {

		StringBuilder sb = new StringBuilder();

		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String json = gson.toJson(finding);
		LOGGER.info("tk json " + json);

		HttpURLConnection conn = null;

		try {

			URL urlVal = new URL(Constantes.URL_FINDING_CLOSING);
			conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);

			OutputStreamWriter dout = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
			dout.write(json);
			dout.flush();
			dout.close();

			int responseCode = conn.getResponseCode();
			LOGGER.info("tk responseCode " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String jsonResponse = sb.toString();

			Type findingType = new TypeToken<Finding>() {
			}.getType();

			Finding findingClosed = gson.fromJson(jsonResponse, findingType);
			return findingClosed;

		} catch (IOException exception) {
			throw new Exception(exception);
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

	public static String getBase64File(File file) {

		try {
			byte[] bytesArray = new byte[(int) file.length()];

			FileInputStream fis = new FileInputStream(file);
			fis.read(bytesArray);
			fis.close();

			Base64 base64 = new Base64();
			String encodedString = new String(base64.encode(bytesArray));
			return encodedString;

		} catch (IOException e) {
			LOGGER.error(e);
		}

		return "";
	}

	public static List<Finding> findAll() {

		StringBuilder sb = new StringBuilder();
		try {

			URL urlVal = new URL(Constantes.URL_FINDING_FINDALL);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

		} catch (IOException e) {
			LOGGER.error(e);
		}

		String json = sb.toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type auditType = new TypeToken<ArrayList<Finding>>() {
		}.getType();

		List<Finding> findings = gson.fromJson(json, auditType);

		return findings;
	}

	public static String exportar() {

		StringBuilder sb = new StringBuilder();
		try {

			URL urlVal = new URL(Constantes.URL_FINDING_EXPORTAR);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

		} catch (IOException e) {
			LOGGER.error(e);
		}

		String json = sb.toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type auditType = new TypeToken<String>() {
		}.getType();

		String file = gson.fromJson(json, auditType);

		return file;
	}

	public static List<Finding> findByNumeroHallazgo(String numeroHallazgo) {

		StringBuilder sb = new StringBuilder();
		String serviceUrl = Constantes.URL_FINDING_FINDBYNUMEROHALLAZGO + "/" + numeroHallazgo;

		try {

			URL urlVal = new URL(serviceUrl);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

		} catch (IOException e) {
			LOGGER.error(e);
		}

		String json = sb.toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type auditType = new TypeToken<ArrayList<Finding>>() {
		}.getType();

		List<Finding> findings = gson.fromJson(json, auditType);
		LOGGER.info("consulta servicio :" + findings);

		return findings;
	}

	public static List<Audit> findAuditsAll() {

		StringBuilder sb = new StringBuilder();
		try {

			URL urlVal = new URL(Constantes.URL_AUDIT_FINDALL);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

		} catch (IOException e) {
			LOGGER.error(e);
		}

		String json = sb.toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type auditType = new TypeToken<ArrayList<Audit>>() {
		}.getType();

		List<Audit> audits = gson.fromJson(json, auditType);
		LOGGER.info("consulta servicio :" + json);

		return audits;
	}

	public static Audit findAuditLast() {

		StringBuilder sb = new StringBuilder();
		try {

			URL urlVal = new URL(Constantes.URL_AUDIT_FINDLAST);

			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

		} catch (IOException e) {
			LOGGER.error(e);
		}

		String json = sb.toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type auditType = new TypeToken<Audit>() {
		}.getType();

		Audit audit = gson.fromJson(json, auditType);
		LOGGER.info("consulta servicio :" + json);

		return audit;
	}

	public static List<Follow> findFollow() {

		StringBuilder sb = new StringBuilder();
		try {

			URL urlVal = new URL(Constantes.URL_FOLLOW_FINDALL);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

		} catch (IOException e) {
			LOGGER.error(e);
		}

		String json = sb.toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type seguimientoType = new TypeToken<ArrayList<Follow>>() {
		}.getType();

		List<Follow> follow = gson.fromJson(json, seguimientoType);
		LOGGER.info("seguimiento servicio :" + follow);

		return follow;
	}

	public static Audit findAuditsById(Integer auditsId) {

		StringBuilder sb = new StringBuilder();
		String urlService = Constantes.URL_AUDIT_ADD + "/" + auditsId;

		try {

			URL urlVal = new URL(urlService);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String json = sb.toString();

			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			Type auditsType = new TypeToken<Audit>() {
			}.getType();

			Audit audits = gson.fromJson(json, auditsType);
			return audits;

		} catch (IOException e) {
			LOGGER.error(e);
		}

		return null;
	}

	public static Finding findFindingById(Integer findingId) throws Exception {

		try {
			StringBuilder sb = new StringBuilder();
			String urlService = Constantes.URL_FINDING_FINDBYID + "/" + findingId;
			URL urlVal = new URL(urlService);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String json = sb.toString();

			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			Type findingsType = new TypeToken<Finding>() {
			}.getType();

			Finding finding = gson.fromJson(json, findingsType);
			return finding;

		} catch (IOException e) {
			throw new Exception(e);
		}
	}

	public static boolean addFollow(Follow follow) throws Exception {

		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String json = gson.toJson(follow);
		LOGGER.info("tk json FOLLOW => " + json);

		HttpURLConnection conn = null;

		try {
			LOGGER.info(Constantes.URL_FOLLOW_ADD);
			URL urlVal = new URL(Constantes.URL_FOLLOW_ADD);
			conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);

			OutputStreamWriter dout = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
			dout.write(json);
			dout.flush();
			dout.close();

			int responseCode = conn.getResponseCode();
			LOGGER.info("tk: respuesta FOLLOW => " + responseCode);
			return true;
		} catch (IOException exception) {
			throw new Exception(exception);
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}
	}

	public static List<Progreso> findProgreso() {

		StringBuilder sb = new StringBuilder();
		try {

			URL urlVal = new URL(Constantes.URL_PROGRESO_FINDALL);

			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

		} catch (IOException e) {
			LOGGER.error(e);
		}

		String json = sb.toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		Type progresoType = new TypeToken<ArrayList<Progreso>>() {
		}.getType();

		List<Progreso> progreso = gson.fromJson(json, progresoType);

		return progreso;
	}

	public static List<Progreso> findProgresosByFindingId(Integer findingId) {

		StringBuilder sb = new StringBuilder();
		String urlService = Constantes.URL_PROGRESO_FINDBYFINDINGID + "/" + findingId;

		try {

			URL urlVal = new URL(urlService);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String json = sb.toString();

			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			Type progresoType = new TypeToken<List<Progreso>>() {
			}.getType();

			List<Progreso> progreso = gson.fromJson(json, progresoType);

			return progreso;

		} catch (IOException e) {
			LOGGER.error(e);
		}

		return null;
	}

	public static List<Follow> findFollowsByFindingId(Integer findingId) {

		StringBuilder sb = new StringBuilder();
		String urlService = Constantes.URL_FOLLOW_FINDBYFINDINGID + "/" + findingId;

		try {

			URL urlVal = new URL(urlService);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String json = sb.toString();

			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			Type followType = new TypeToken<List<Follow>>() {
			}.getType();

			List<Follow> follow = gson.fromJson(json, followType);
			return follow;

		} catch (IOException e) {
			LOGGER.error(e);
		}

		return null;
	}

	public static boolean addProgreso(Progreso progreso) {

		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String json = gson.toJson(progreso);

		HttpURLConnection conn = null;

		try {
			LOGGER.info(Constantes.URL_PROGRESO_ADD);
			URL urlVal = new URL(Constantes.URL_PROGRESO_ADD);
			conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);

			OutputStreamWriter dout = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
			dout.write(json);
			dout.flush();
			dout.close();

			int responseCode = conn.getResponseCode();
			LOGGER.info("tk: respuesta PROGRESO => " + responseCode);
			return true;
		} catch (IOException exception) {
			LOGGER.error(exception);
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		return false;
	}

	public static boolean deleteFollow(Integer id) {

		StringBuilder sb = new StringBuilder();
		String serviceURL = Constantes.URL_CONSUMO_FOLLOW_DELETE + "/" + id;

		try {

			URL urlVal = new URL(serviceURL);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestMethod("GET");

			int responseCode = conn.getResponseCode();

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			return true;

		} catch (Exception e) {
			LOGGER.error(e);
		}

		return false;

	}


	public static boolean cloneFinding(Integer id) {

		StringBuilder sb = new StringBuilder();
		String serviceURL = Constantes.URL_FINDING_AMPLIAR + "/" + id;

		try {

			URL urlVal = new URL(serviceURL);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestMethod("GET");

			int responseCode = conn.getResponseCode();

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			return true;

		} catch (Exception e) {
			LOGGER.error(e);
		}

		return false;

	}

	public static boolean isExpired(Date fechaTerminacion) {

		Calendar calendarWeek = CalendarFactoryUtil.getCalendar();

		calendarWeek.add(Calendar.DAY_OF_YEAR, 7);

		Calendar calendarFechaTerminacion = CalendarFactoryUtil.getCalendar();
		calendarFechaTerminacion.setTime(fechaTerminacion);

		if (calendarWeek.getTime().getTime() >= fechaTerminacion.getTime()) {
			return true;
		}

		return false;

	}

}
