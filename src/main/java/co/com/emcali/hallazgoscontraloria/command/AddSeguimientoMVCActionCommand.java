package co.com.emcali.hallazgoscontraloria.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import co.com.emcali.hallazgoscontraloria.constants.HallazgosContraloriaPortletKeys;
import co.com.emcali.hallazgoscontraloria.model.Finding;
import co.com.emcali.hallazgoscontraloria.model.Progreso;
import co.com.emcali.hallazgoscontraloria.utils.HallazgosContraloriaUtil;

/**
 * @author salek
 */
@Component(immediate = true, property = {
		"javax.portlet.name=" + HallazgosContraloriaPortletKeys.HALLAZGOSCONTRALORIACONSULTA,
		"mvc.command.name=/hallazgoscontraloria/addSeguimiento" }, service = MVCActionCommand.class)

public class AddSeguimientoMVCActionCommand extends BaseMVCActionCommand {

	private Log _log = LogFactoryUtil.getLog(AddSeguimientoMVCActionCommand.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Integer findingId = ParamUtil.getInteger(actionRequest, "findingId");
		// editable
		float advance = ParamUtil.getFloat(actionRequest, "advance");
		Date fechaAvance = ParamUtil.getDate(actionRequest, "fecha", sdf);
		String descripcion = ParamUtil.getString(actionRequest, "descripcion");
		String observacion = ParamUtil.getString(actionRequest, "observacion");
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		File file = uploadPortletRequest.getFile("documentoAnexo");

		try {

			Finding finding = HallazgosContraloriaUtil.findFindingById(findingId);
			Progreso progreso = new Progreso();
			progreso.setDescripcion(descripcion);
			progreso.setObservaciones(observacion);
			if (file.length() > 0) {
				progreso.setAdjunto(HallazgosContraloriaUtil.getBase64File(file));
			}
			progreso.setAvance(advance);
			progreso.setFechaAvance(fechaAvance);
			progreso.setFinding(finding);
			HallazgosContraloriaUtil.addProgreso(progreso);
			actionResponse.setRenderParameter("mvcRenderCommandName", "/hallazgoscontraloria/listadoSeguimiento");

		} catch (Exception e) {
			_log.error(e);
			SessionErrors.add(actionRequest, "error-add");
			actionResponse.setRenderParameter("mvcRenderCommandName", "/hallazgoscontraloria/addSeguimiento");
		}
	}
}