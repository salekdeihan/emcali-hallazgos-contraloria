/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package co.com.emcali.hallazgoscontraloria.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import co.com.emcali.hallazgoscontraloria.constants.HallazgosContraloriaPortletKeys;
import co.com.emcali.hallazgoscontraloria.model.Audit;
import co.com.emcali.hallazgoscontraloria.model.Finding;
import co.com.emcali.hallazgoscontraloria.model.Follow;
import co.com.emcali.hallazgoscontraloria.utils.HallazgosContraloriaUtil;


/**
 * @author salek
 */

@Component(
		immediate = true, 
		property = {
				"javax.portlet.name=" + HallazgosContraloriaPortletKeys.HALLAZGOSCONTRALORIACONSULTA,
				"mvc.command.name=/hallazgoscontraloria/addSeguimiento" },
		service = MVCRenderCommand.class)
public class AddSeguimientoMVCRenderCommand implements MVCRenderCommand {

	private Log _log = LogFactoryUtil.getLog(getClass());

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {

		Integer findingId = ParamUtil.getInteger(renderRequest, "findingId");

		try {
			Finding finding = HallazgosContraloriaUtil.findFindingById(findingId);
			renderRequest.setAttribute("finding", finding);

		} catch (Exception e) {
			_log.error(e);
		}

		return "/jsp/addSeguimiento.jsp";
	}

}
