package co.com.emcali.hallazgoscontraloria.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import co.com.emcali.hallazgoscontraloria.constants.HallazgosContraloriaPortletKeys;
import co.com.emcali.hallazgoscontraloria.model.Finding;
import co.com.emcali.hallazgoscontraloria.utils.HallazgosContraloriaUtil;

/**
 * @author salek
 */
@Component(
		immediate = true, 
		property = {
				"javax.portlet.name=" + HallazgosContraloriaPortletKeys.HALLAZGOSCONTRALORIACONSULTA,
				"mvc.command.name=/hallazgoscontraloria/deleteFollow" }, service = MVCActionCommand.class)

public class DeleteFuenteMVCActionCommand extends BaseMVCActionCommand {

	private Log _log = LogFactoryUtil.getLog(DeleteFuenteMVCActionCommand.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {

		Integer followId = ParamUtil.getInteger(actionRequest, "followId");
		Integer findingId = ParamUtil.getInteger(actionRequest, "findingId");

		try {
			
			Finding finding = HallazgosContraloriaUtil.findFindingById(findingId);
			actionRequest.setAttribute("finding", finding);

			HallazgosContraloriaUtil.deleteFollow(followId);
	
		} catch (Exception e) {
			_log.error(e);
			SessionErrors.add(actionRequest, "error-delete");
		}

		actionResponse.setRenderParameter("mvcRenderCommandName", "/hallazgoscontraloria/listadoFuentes");
	}
}