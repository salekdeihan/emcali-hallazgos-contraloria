package co.com.emcali.hallazgoscontraloria.command;

import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import co.com.emcali.hallazgoscontraloria.constants.HallazgosContraloriaPortletKeys;
import co.com.emcali.hallazgoscontraloria.model.Audit;
import co.com.emcali.hallazgoscontraloria.model.Finding;
import co.com.emcali.hallazgoscontraloria.model.Follow;
import co.com.emcali.hallazgoscontraloria.utils.HallazgosContraloriaFormUtil;
import co.com.emcali.hallazgoscontraloria.utils.HallazgosContraloriaUtil;

/**
 * @author salek
 */
@Component(immediate = true, property = {
		"javax.portlet.name=" + HallazgosContraloriaPortletKeys.HALLAZGOSCONTRALORIACONSULTA,
		"mvc.command.name=/hallazgoscontraloria/saveRegistro" }, service = MVCActionCommand.class)

public class AddRegistroMVCActionCommand extends BaseMVCActionCommand {

	private Log _log = LogFactoryUtil.getLog(AddRegistroMVCActionCommand.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String numeroHallazgo = ParamUtil.getString(actionRequest, "numeroHallazgo");
		String connotacionHallazgo = ParamUtil.getString(actionRequest, "connotacionHallazgo");
		String macroproceso = ParamUtil.getString(actionRequest, "macroproceso");
		String accionCorrectiva = ParamUtil.getString(actionRequest, "accionCorrectiva");
		String meta = ParamUtil.getString(actionRequest, "meta");
		String fuenteVerificacion = ParamUtil.getString(actionRequest, "fuenteVerificacion");
		String lugarVerificacion = ParamUtil.getString(actionRequest, "lugarVerificacion");
		Date fechaTerminacion = ParamUtil.getDate(actionRequest, "fechaTerminacion", sdf);
		String descripcion = ParamUtil.getString(actionRequest, "descripcion");
		String responsable = ParamUtil.getString(actionRequest, "responsable");
		String email = ParamUtil.getString(actionRequest, "email");

		String entidad = ParamUtil.getString(actionRequest, "entidad");
		String ejercicioNombre = ParamUtil.getString(actionRequest, "ejercicioNombre");
		String ejercicioTipo = ParamUtil.getString(actionRequest, "ejercicioTipo");
		String vigencia = ParamUtil.getString(actionRequest, "vigencia");
		Date fechaSuscripcion = ParamUtil.getDate(actionRequest, "fechaSuscripcion", sdf);

		try {

			// AUDIT
			Audit audit = new Audit();
			audit.setEntidad(entidad);
			audit.setEjercicioTipo(ejercicioTipo);
			audit.setEjercicioNombre(ejercicioNombre);
			audit.setVigencia(vigencia);
			audit.setFecha(fechaSuscripcion);
			Audit newAudit = HallazgosContraloriaUtil.addAudit(audit);

			// FINDING
			Finding finding = new Finding();
			finding.setCodigo(numeroHallazgo);
			finding.setConnotacion(connotacionHallazgo);
			finding.setAccion(accionCorrectiva);
			finding.setMacroproceso(macroproceso);
			finding.setTerminacion(fechaTerminacion);
			finding.setMeta(meta);
			finding.setDescripcion(descripcion);
			finding.setEmail(email);

			finding.setDireccion("");
			finding.setEfectividad(0);
			finding.setCumplimiento(0);
			finding.setEstado("1");

			finding.setAudit(newAudit);
			Finding newFinding = HallazgosContraloriaUtil.addRegistro(finding);

			// FOLLOW
			Follow follow = new Follow();
			follow.setFinding(newFinding);
			follow.setFuente(fuenteVerificacion);
			follow.setLugar(lugarVerificacion);
			follow.setResponsable(responsable);
			HallazgosContraloriaUtil.addFollow(follow);

			// Envio Email

			_log.info("....INICIO ENVIANDO CORREO...");

			HallazgosContraloriaFormUtil.sendEmailPersonal("", email, newAudit.getAuditoria(), numeroHallazgo);

			_log.info("....FIN CORREO...");

			actionResponse.setRenderParameter("mvcRenderCommandName", "/hallazgoscontraloria/listadoRegistro");

		} catch (Exception e) {
			_log.error(e);
			SessionErrors.add(actionRequest, "error-add");
			actionResponse.setRenderParameter("mvcRenderCommandName", "/hallazgoscontraloria/addRegistro");
		}
	}
}