package co.com.emcali.hallazgoscontraloria.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import co.com.emcali.hallazgoscontraloria.constants.HallazgosContraloriaPortletKeys;
import co.com.emcali.hallazgoscontraloria.model.Finding;
import co.com.emcali.hallazgoscontraloria.model.Follow;
import co.com.emcali.hallazgoscontraloria.utils.HallazgosContraloriaUtil;

/**
 * @author salek
 */
@Component(immediate = true, property = {
		"javax.portlet.name=" + HallazgosContraloriaPortletKeys.HALLAZGOSCONTRALORIACONSULTA,
		"mvc.command.name=/hallazgoscontraloria/addFollow" }, service = MVCActionCommand.class)

public class AddFuenteMVCActionCommand extends BaseMVCActionCommand {

	private Log _log = LogFactoryUtil.getLog(AddFuenteMVCActionCommand.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {

		Integer findingId = ParamUtil.getInteger(actionRequest, "findingId");

		String fuenteVerificacion = ParamUtil.getString(actionRequest, "fuenteVerificacion");
		String lugarVerificacion = ParamUtil.getString(actionRequest, "lugarVerificacion");
		String responsable = ParamUtil.getString(actionRequest, "responsable");

		try {

			Finding finding = HallazgosContraloriaUtil.findFindingById(findingId);

			// FOLLOW
			Follow follow = new Follow();
			follow.setFuente(fuenteVerificacion);
			follow.setLugar(lugarVerificacion);
			follow.setFinding(finding);
			follow.setResponsable(responsable);
			HallazgosContraloriaUtil.addFollow(follow);
			actionResponse.setRenderParameter("mvcRenderCommandName", "/hallazgoscontraloria/listadoFuentes");

		} catch (Exception e) {
			_log.error(e);
			SessionErrors.add(actionRequest, "error-add");
			actionResponse.setRenderParameter("mvcRenderCommandName", "/hallazgoscontraloria/addFuente");
		}
	}

}