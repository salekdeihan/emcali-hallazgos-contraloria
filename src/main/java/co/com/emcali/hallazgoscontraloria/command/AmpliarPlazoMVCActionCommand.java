package co.com.emcali.hallazgoscontraloria.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import co.com.emcali.hallazgoscontraloria.constants.HallazgosContraloriaPortletKeys;
import co.com.emcali.hallazgoscontraloria.utils.HallazgosContraloriaUtil;

/**
 * @author salek
 */
@Component(
		immediate = true, 
		property = {
				"javax.portlet.name=" + HallazgosContraloriaPortletKeys.HALLAZGOSCONTRALORIACONSULTA,
				"mvc.command.name=/hallazgoscontraloria/ampliarPlazoRegistros" }, service = MVCActionCommand.class)

public class AmpliarPlazoMVCActionCommand extends BaseMVCActionCommand {

	private Log _log = LogFactoryUtil.getLog(AmpliarPlazoMVCActionCommand.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {

		Integer findingId = ParamUtil.getInteger(actionRequest, "findingId");
		
		try {
			_log.info("zkkkk11");
			HallazgosContraloriaUtil.cloneFinding(findingId);
			_log.info("zkkkk22");

		} catch (Exception e) {
			_log.error(e);
			SessionErrors.add(actionRequest, "error-ampliar");
		}

		actionResponse.setRenderParameter("mvcRenderCommandName", "/hallazgoscontraloria/listadoRegistro");
	}
}