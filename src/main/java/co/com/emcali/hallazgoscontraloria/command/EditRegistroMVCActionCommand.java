package co.com.emcali.hallazgoscontraloria.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import co.com.emcali.hallazgoscontraloria.constants.HallazgosContraloriaPortletKeys;
import co.com.emcali.hallazgoscontraloria.model.Finding;
import co.com.emcali.hallazgoscontraloria.utils.HallazgosContraloriaUtil;

/**
 * @author salek
 */
@Component(immediate = true, property = {
		"javax.portlet.name=" + HallazgosContraloriaPortletKeys.HALLAZGOSCONTRALORIACONSULTA,
		"mvc.command.name=/hallazgoscontraloria/editRegistro" }, service = MVCActionCommand.class)

public class EditRegistroMVCActionCommand extends BaseMVCActionCommand {

	private Log _log = LogFactoryUtil.getLog(EditRegistroMVCActionCommand.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {

		Integer findingId = ParamUtil.getInteger(actionRequest, "findingId");
		String direccion = ParamUtil.getString(actionRequest, "direccion");
		Integer efectividad = ParamUtil.getInteger(actionRequest, "efectividad");
		Integer cumplimiento = ParamUtil.getInteger(actionRequest, "cumplimiento");
		String estado = ParamUtil.getString(actionRequest, "estado");
		_log.info("finding id = " + findingId);

		try {
			// FINDING
			Finding finding = HallazgosContraloriaUtil.findFindingById(findingId);
			finding.setDireccion(direccion);
			finding.setEfectividad(efectividad);
			finding.setCumplimiento(cumplimiento);
			finding.setEstado(estado);
			actionRequest.setAttribute("finding", finding);

			HallazgosContraloriaUtil.closingRegistro(finding);
			actionResponse.setRenderParameter("mvcRenderCommandName", "/hallazgoscontraloria/listadoRegistro");
			
		} catch (Exception e) {
			_log.error(e);
			SessionErrors.add(actionRequest, "error-add");
			actionResponse.setRenderParameter("mvcRenderCommandName", "/hallazgoscontraloria/editRegistro");
		}
	}
}