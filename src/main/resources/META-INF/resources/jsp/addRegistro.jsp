<%@ include file="./init.jsp"%>

<liferay-portlet:actionURL name="/hallazgoscontraloria/saveRegistro" var="saveRegistro" />

<liferay-portlet:renderURL var="consultar">
	<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/listadoRegistro" />
</liferay-portlet:renderURL>

<div class="emcali-hallazgos-contraloria">
	<liferay-ui:error key="error-add" message="emcali.hallazgoscontraloria.registro.add.error-add" />
	<section class="form-register">
		<h2><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.mensaje"/></h2>
		<br />
		<h4><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.entidad"/></h4>
		<form method="post" action="<%=saveRegistro.toString()%>">
		<div class="form-row">
			<div class="col-md-4">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.nombre-entidad"/></label> 
				<input id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>entidad" >
			</div>
			<div class=" col-sm-4">
				<label for="PVFCT"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.pvfct"/></label> 
				<input id="PVFCT" type="text" class="form-control" name="<portlet:namespace/>vigencia" >
			</div>
			<div class="col-md-4">
				<label for="ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.fecha-suscripcion"/></label> 
				<input id="auditoriaDesempeno" type="date" class="form-control" name="<portlet:namespace/>fechaSuscripcion">
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-6">
				<label for="nombre_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.nombre-ejericio"/></label> 
				<input id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>ejercicioNombre" >
			</div>
			<div class="col-md-6">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.tipo-ejericio"/></label> 
				<input id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>ejercicioTipo" >
			</div>
		</div>
		<br />
		<h4><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.registro"/></h4>
			<div class="form-row">
				<div class=" col-md-3 ">
					<label for="entidad"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.numero-hallazgo"/></label> 
					<input id="entidad" type="text" class="form-control" name="<portlet:namespace/>numeroHallazgo">
				</div>
				<div class="col-md-3">
					<label for="ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.connotacion-hallazgo"/></label> 
					<input id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>connotacionHallazgo">
				</div>
				<div class="col-md-2">
					<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.macroproceso"/></label> 
					<input id="tipo_ejercicio" type="text" class="form-control" name="<portlet:namespace/>macroproceso">
				</div>
				<div class="col-md-3">
					<label for="fecha"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.accion-correctiva"/></label> 
					<input id="fecha" type="text" class="form-control" name="<portlet:namespace/>accionCorrectiva">
				</div>
				<div class="col-md-1">
					<label for="PVFCT"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.meta"/></label>
					<input id="PVFCT" type="text" class="form-control" name="<portlet:namespace/>meta">
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6">
					<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.descripcion"/></label>
					<textarea id="descripcion" class="form-control" placeholder="Condicion y causa" rows="1" name="<portlet:namespace/>descripcion"></textarea>
				</div>
				<div class="col-md-2">
					<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.fecha-terminacion"/></label>
					<input id="tipo_ejercicio" type="date" class="form-control" name="<portlet:namespace/>fechaTerminacion">
				</div>
				<div class="col-md-4">
					<label for="email"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.email"/></label>
					<input id="email" type="email" class="form-control" name="<portlet:namespace/>email" multiple>
				</div>
			</div>
			<br />
			<h4>Fuente inicial</h4>
			<div class="form-row">
				<div class="col-md-4">
					<label for="entidad"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.fuente-verificacion"/></label>
					<input id=entidad type="text" class="form-control" name="<portlet:namespace/>fuenteVerificacion">
				</div>
				<div class="col-md-4">
					<label for="entidad"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.lugar-verificacion"/></label>
					<input id=entidad type="text" class="form-control" name="<portlet:namespace/>lugarVerificacion">
				</div>
				<div class="col-md-4">
					<label for="fecha"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.responsable"/></label> 
					<input id="fecha" type="text" class="form-control" name="<portlet:namespace/>responsable">
				</div>
			</div>
			<br />
			<div class="form-row">
				<div class="col-md-12">
					<button onclick='return confirm("�Est�s seguro que deseas guardar?")' class="botons" id="crear"><liferay-ui:message key="emcali.hallazgoscontraloria.general.guardar"/></button>
					<a class="btn" href="<%=consultar.toString() %>" ><liferay-ui:message key="emcali.hallazgoscontraloria.general.cancelar"/></a>
				</div>
			</div>
		</form>
	</section>
</div>