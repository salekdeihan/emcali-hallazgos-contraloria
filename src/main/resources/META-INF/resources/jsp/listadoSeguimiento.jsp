<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@ include file="./init.jsp"%>
<%
	List<Progreso> progresos = (List<Progreso>) request.getAttribute("progresos");
	Finding finding = (Finding) renderRequest.getAttribute("finding");

%>

<liferay-portlet:renderURL var="addSeguimiento">
	<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/addSeguimiento" />
	<liferay-portlet:param name="findingId" value="<%=String.valueOf(finding.getId()) %>"/>
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="consultar">
	<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/listadoRegistro" />
</liferay-portlet:renderURL>

<div class="emcali-hallazgos-contraloria">
	<liferay-ui:error key="error-add"
		message="No se ha guardado el seguimiento." />
		
<div class="emcali-hallazgos-contraloria">
	<section class="form-register">
		<h2><liferay-ui:message key="emcali.hallazgoscontraloria.seguimiento.listado.mensaje"/></h2>
		<br />
		<div class="form-group col-md-12">
			<a class="btn btn-primary btn-default botons" href="<%=consultar.toString() %>"><liferay-ui:message key="emcali.hallazgoscontraloria.general.atras"/></a>
			<a class="btn btn-primary btn-default botons" id="adicionar" href="<%=addSeguimiento.toString()%>"><liferay-ui:message key="emcali.hallazgoscontraloria.general.adicionar"/></a>
		</div>
		<div class="form-group">
			<div class="form-row table-responsive">
				<table id="TABLA" class="table table-hover ">
					<thead>
						<tr>
							<th><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.numero-hallazgo"/></th>
							<th><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.fecha"/></th>
							<th><liferay-ui:message key="emcali.hallazgoscontraloria.seguimiento.add.descripcion"/></th>
							<th><liferay-ui:message key="emcali.hallazgoscontraloria.seguimiento.add.observacion"/></th>
							<th><liferay-ui:message key="emcali.hallazgoscontraloria.seguimiento.add.avance"/></th>
							<th><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.archivo-cargado"/></th>
						</tr>
					</thead>
					<tbody>
					<%
					for(Progreso progreso : progresos){
					%>
						<tr>
							<td><%=progreso.getFinding().getCodigo()%></td>
							<td><%=sdf.format(progreso.getFechaAvance())%></td>
							<td><%=progreso.getDescripcion()%></td>
							<td><%=progreso.getObservaciones()%></td>
							<td><%=progreso.getAvance()%></td>
							<td><%=progreso.getFinding().getEfectividad()%></td>
							<td><%=progreso.getFinding().getCumplimiento()%></td>
							<td>
								<%if(Validator.isNotNull(progreso.getAdjunto()) && !progreso.getAdjunto().isEmpty()) {%>
									<a class="btn btn-primary btn-default botons" href="data:application/pdf;base64,<%=progreso.getAdjunto() %>" download="adjunto-seguimiento.pdf"><liferay-ui:message key="emcali.hallazgoscontraloria.general.descargar"/></a>
								<%}%>
							</td>
						</tr>
					<%
					}
					%>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
