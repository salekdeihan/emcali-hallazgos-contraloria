<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="co.com.emcali.hallazgoscontraloria.model.Finding"%>
<%@page import="co.com.emcali.hallazgoscontraloria.model.Follow"%>
<%@page import="co.com.emcali.hallazgoscontraloria.model.Audit"%>
<%@page import="co.com.emcali.hallazgoscontraloria.model.Progreso"%>
<%@page import="co.com.emcali.hallazgoscontraloria.constants.Constantes"%>
<%@page import="co.com.emcali.hallazgoscontraloria.utils.HallazgosContraloriaUtil"%>


<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat sdfInputDateValue = new SimpleDateFormat("yyyy-MM-dd");
%>