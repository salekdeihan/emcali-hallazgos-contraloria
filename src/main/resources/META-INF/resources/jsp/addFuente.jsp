<%@ include file="./init.jsp"%>

<%
	Finding finding = (Finding) renderRequest.getAttribute("finding");
	Audit audit = finding.getAudit();
%>
<liferay-portlet:actionURL name="/hallazgoscontraloria/addFollow" var="addFollow" />

<liferay-portlet:renderURL var="listadoFuentes">
	<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/listadoFuentes" />
	<liferay-portlet:param name="findingId" value="<%=String.valueOf(finding.getId()) %>"/>
</liferay-portlet:renderURL>

<div class="emcali-hallazgos-contraloria">
	<liferay-ui:error key="error-add" message="emcali.hallazgoscontraloria.fuente.add.error-add" />
	<section class="form-register">
		<h2><liferay-ui:message key="emcali.hallazgoscontraloria.fuente.add.mensaje"/></h2>
		<br />
		<h4><liferay-ui:message key="emcali.hallazgoscontraloria.fuente.add.detalle-hallazgo"/></h4>
		<div class="form-row">
			<div class="col-md-3">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.nombre-entidad"/></label> 
				<input value="<%=audit.getEntidad()%>" id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>tipoEjercicio" disabled="disabled">
			</div>
			<div class="col-md-3">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.tipo-ejericio"/></label> 
				<input value="<%=audit.getEjercicioTipo()%>" id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>tipoEjercicio" disabled="disabled">
			</div>
			<div class="col-md-3">
				<label for="ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.auditorio-desempeno"/></label> 
				<input value="<%=audit.getAuditoria()%>" id="auditoriaDesempeno" type="text" class="form-control" name="<portlet:namespace/>auditoriaDesempeno" disabled="disabled">
			</div>
			<div class=" col-sm-3">
				<label for="PVFCT"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.pvfct"/></label> 
				<input value="<%=audit.getVigencia()%>" id="PVFCT" type="text" class="form-control" name="<portlet:namespace/>pvfct" disabled="disabled">
			</div>
		</div>
		<div class="form-row">
			<div class=" col-md-3 ">
				<label for="entidad"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.numero-hallazgo"/></label> 
				<input value="<%=finding.getCodigo() %>" id="entidad" type="text" class="form-control" name="<portlet:namespace/>numeroHallazgo" disabled="disabled">
			</div>
			<div class="col-md-3">
				<label for="ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.connotacion-hallazgo"/></label> 
				<input value="<%=finding.getConnotacion() %>" id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>connotacionHallazgo" disabled="disabled">
			</div>
			<div class="col-md-2">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.macroproceso"/></label> 
				<input value="<%=finding.getMacroproceso() %>" id="tipo_ejercicio" type="text" class="form-control" name="<portlet:namespace/>macroproceso" disabled="disabled">
			</div>
			<div class="col-md-3">
				<label for="fecha"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.accion-correctiva"/></label> 
				<input value="<%=finding.getAccion() %>" id="fecha" type="text" class="form-control" name="<portlet:namespace/>accionCorrectiva" disabled="disabled">
			</div>
			<div class="col-md-1">
				<label for="PVFCT"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.meta"/></label>
				<input value="<%=finding.getMeta() %>" id="PVFCT" type="text" class="form-control" name="<portlet:namespace/>meta" disabled="disabled">
			</div>
		</div>
		<br/>
		<h4><liferay-ui:message key="emcali.hallazgoscontraloria.fuente.add.detalle-fuente"/></h4>
		<form method="post" action="<%=addFollow.toString()%>">
			<input type="hidden" name='<portlet:namespace/>findingId' value="<%=finding.getId() %>" />
			<div class="form-row">
				<div class="form-group col-md-4">
					<label for="entidad"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.fuente-verificacion"/></label> 
					<input id=entidad type="text" class="form-control" name="<portlet:namespace/>fuenteVerificacion">
				</div>
				<div class="form-group col-md-4">
					<label for="entidad"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.lugar-verificacion"/></label>
					<input id=entidad type="text" class="form-control" name="<portlet:namespace/>lugarVerificacion">
				</div>
				<div class="form-group col-md-4">
					<label for="entidad"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.responsable"/></label> 
					<input id=entidad type="text" class="form-control" name="<portlet:namespace/>responsable">
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6 ">
					<button class="botons" id="confirmar"><liferay-ui:message key="emcali.hallazgoscontraloria.general.guardar"/></button>
					<a href="<%=listadoFuentes.toString() %>" class="btn"><liferay-ui:message key="emcali.hallazgoscontraloria.general.cancelar"/></button>
				</div>
			</div>
		</form>
	</section>
</div>