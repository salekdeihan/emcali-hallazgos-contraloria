<%@ include file="./init.jsp"%>

<%
	List<Finding> findings = (List<Finding>) request.getAttribute("findings");
%>

<liferay-portlet:renderURL var="addRegistro">
	<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/addRegistro" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="consultar">
	<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/listadoRegistro" />
</liferay-portlet:renderURL>

<div class="emcali-hallazgos-contraloria">
	<liferay-ui:error key="error-ampliar" message="emcali.hallazgoscontraloria.registro.add.error-ampliar" />
	<section class="form-register">
		<h2><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.mensaje"/></h2>
		<br />
		<form method="post" action="<%=consultar.toString()%>">
			<div class="form-row" id="formulario">
				<div class="col-md-4">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Buscar por n�mero de hallazgos" name="<portlet:namespace/>numeroHallazgoBusqueda">
					</div>
				</div>
				<div class="col-md-2">
					<button class="btn btn-primary btn-default botons"><liferay-ui:message key="emcali.hallazgoscontraloria.general.buscar"/></button>
				</div>
				<div class="col-md-2 align-right">
					<a class="btn btn-primary btn-default botons app-btn" id="adicionar" href="<%=addRegistro.toString()%>"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.add"/></a>
				</div>
				<div class="col-md-2">
					<a class="btn btn-primary btn-default botons app-btn"  target="_blank" href="<%=Constantes.URL_DESCARGAR_PLANTILLA%>" id="descargar"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.descargar"/></a>
				</div>
			</div>
		</form>
		<br />
		<div class="form-row table-responsive">
			<table id="TABLA" class="table table-hover">
				<thead>
					<tr>
						<th></th>
						<th><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.numero-hallazgo"/></th>
						<th><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.entidad"/></th>
						<th><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.nombre-ejericio"/></th>
						<th><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.connotacion-hallazgo"/></th>
						<th><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.macroproceso"/></th>
						<th><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.accion-correctiva"/></th>
						<th><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.meta"/></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<%
						for (Finding busqueda : findings) {
					%>
						<liferay-portlet:renderURL var="listadoSeguimiento">
							<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/listadoSeguimiento" />
							<liferay-portlet:param name="findingId" value="<%=String.valueOf(busqueda.getId()) %>"/>
						</liferay-portlet:renderURL>
						<liferay-portlet:renderURL var="verDetalleRegistro">
							<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/verDetalleRegistro" />
							<liferay-portlet:param name="findingId" value="<%=String.valueOf(busqueda.getId()) %>"/>
						</liferay-portlet:renderURL>
						<liferay-portlet:renderURL var="editRegistro">
							<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/editRegistro" />
							<liferay-portlet:param name="findingId" value="<%=String.valueOf(busqueda.getId()) %>"/>
						</liferay-portlet:renderURL>
						<liferay-portlet:renderURL var="listadoFuentes">
							<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/listadoFuentes" />
							<liferay-portlet:param name="findingId" value="<%=String.valueOf(busqueda.getId()) %>"/>
						</liferay-portlet:renderURL>
						<liferay-portlet:actionURL name="/hallazgoscontraloria/ampliarPlazoRegistros" var="ampliarPlazoRegistro">
							<liferay-portlet:param name="findingId" value="<%=String.valueOf(busqueda.getId()) %>"/>
						</liferay-portlet:actionURL>
						
						<tr>
							<td>
								<%if(HallazgosContraloriaUtil.isExpired(busqueda.getTerminacion())){ %>
									<img src="<%=request.getContextPath() %>/images/caducidad.png">
								<%} %>
							</td>
							<td><%=busqueda.getCodigo()%></td>
							<td><%=busqueda.getAudit().getEntidad()%></td>
							<td><%=busqueda.getAudit().getEjercicioNombre()%></td>
							<td><%=busqueda.getConnotacion()%></td>
							<td><%=busqueda.getMacroproceso()%></td>
							<td><%=busqueda.getAccion()%></td>
							<td><%=busqueda.getMeta()%></td>
							<td>
								<div class="btn-group">
									<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Acciones</button>
									<div class="dropdown-menu">
										<a class="dropdown-item" href="<%=verDetalleRegistro.toString()%>"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.ver-detalle"/></a> 
										<a class="dropdown-item" href="<%=editRegistro.toString()%>"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.edit"/></a> 
										<a class="dropdown-item" href="<%=ampliarPlazoRegistro.toString()%>"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.ampliarPlazoRegistro"/></a> 
										<a class="dropdown-item" href="<%=listadoSeguimiento.toString()%>"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.seguimiento"/></a> 
										<a class="dropdown-item" href="<%=listadoFuentes.toString()%>"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.fuentes"/></a>
							    	</div>
								</div>
							</td>
						</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
		<div class="form-row align-right">
			<div class="col-md-6">
				<a target="_blank" href="<%=Constantes.URL_AUDIT_EXPORT%>" id="exportar" class="btn btn-primary btn-default botons"><liferay-ui:message key="emcali.hallazgoscontraloria.general.exportar"/></a>
			</div>
		</div>
	</section>
</div>