<%@ include file="./init.jsp"%>
<%
	List<Follow> follows = (List<Follow>) request.getAttribute("follows");
	Finding finding = (Finding) renderRequest.getAttribute("finding");
%>

<liferay-portlet:renderURL var="addFuente">
	<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/addFuente" />
	<liferay-portlet:param name="findingId" value="<%=String.valueOf(finding.getId()) %>"/>
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="consultar">
	<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/listadoRegistro" />
</liferay-portlet:renderURL>

<div class="emcali-hallazgos-contraloria">
	<liferay-ui:error key="error-add" message="emcali.hallazgoscontraloria.registro.add.error-add" />
	<section class="form-register">
		<h2><liferay-ui:message key="emcali.hallazgoscontraloria.fuente.add.listado-fuente"/></h2>
		<br />
		<div class="form-group col-md-12">
			<a class="btn btn-primary btn-default botons" href="<%=consultar.toString() %>"><liferay-ui:message key="emcali.hallazgoscontraloria.general.atras"/></a>
			<a class="btn btn-primary btn-default botons" id="adicionar" href="<%=addFuente.toString()%>" ><liferay-ui:message key="emcali.hallazgoscontraloria.general.adicionar"/></a>
		</div>
		<div class="form-group">
			<div class="form-row table">
				<table class="table table-hover ">
					<thead>
						<tr>
							<th><liferay-ui:message key="emcali.hallazgoscontraloria.fuente.add.fuente-verificacion"/></th>
							<th><liferay-ui:message key="emcali.hallazgoscontraloria.fuente.add.lugar-verificacion"/></th>
							<th><liferay-ui:message key="emcali.hallazgoscontraloria.fuente.add.responsable"/></th>
							<th><liferay-ui:message key="emcali.hallazgoscontraloria.fuente.add.estado"/></th>
							<th><liferay-ui:message key="emcali.hallazgoscontraloria.general.accion"/></th>
						</tr>
					</thead>
					<tbody>
					<%
						for(Follow follow : follows){
					%>
						<liferay-portlet:actionURL name="/hallazgoscontraloria/deleteFollow" var="deleteFollow">
							<liferay-portlet:param name="followId" value="<%=String.valueOf(follow.getId()) %>"/>
							<liferay-portlet:param name="findingId" value="<%=String.valueOf(finding.getId()) %>"/>
						</liferay-portlet:actionURL>
						<tr>
							<td><%=follow.getFuente()%></td>
							<td><%=follow.getLugar()%></td>
							<td><%=follow.getResponsable()%></td>
							<td><%=follow.getStatus()%></td>
							<td>
								<div class="btn-group">
									<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Acciones</button>
									<div class="dropdown-menu">
											<a class="dropdown-item" href="<%=deleteFollow.toString()%>"><liferay-ui:message key="emcali.hallazgoscontraloria.general.eliminar"/></a> 
									</div>
								</div>
							</td>
						</tr>
					<%} %>
				</tbody>
				</table>
			</div>
		</div>
	</section>
</div>