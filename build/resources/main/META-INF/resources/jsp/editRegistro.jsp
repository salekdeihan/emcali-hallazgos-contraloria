<%@ include file="./init.jsp"%>

<%
	Finding finding = (Finding) renderRequest.getAttribute("finding");
%>

<liferay-portlet:actionURL name="/hallazgoscontraloria/editRegistro" var="editRegistro" />

<liferay-portlet:renderURL var="consultar">
	<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/listadoRegistro" />
</liferay-portlet:renderURL>

<div class="emcali-hallazgos-contraloria">
	<liferay-ui:error key="error-add" message="emcali.hallazgoscontraloria.registro.add.error-add" />
	<section class="form-register">
		<h2><liferay-ui:message key="emcali.hallazgoscontraloria.registro.edit.mensaje"/></h2>
		<br />
		<h4><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.entidad"/></h4>
		<form method="post" action="<%=editRegistro.toString()%>">
			<input type="hidden" name='<portlet:namespace/>auditId' value="<%=finding.getAudit().getId() %>" />
			<input type="hidden" name='<portlet:namespace/>findingId' value="<%=finding.getId() %>" />
			
			<div class="form-row">
				<div class="col-md-4">
					<label for="entidad"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.nombre-entidad"/></label> 
					<input disabled="disabled" value="<%=finding.getAudit().getEntidad() %>" id="entidad" type="text" class="form-control" name="<portlet:namespace/>entidad" >
				</div>
				<div class="col-sm-4">
					<label for="PVFCT"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.pvfct"/></label> 
					<input disabled="disabled" value="<%=finding.getAudit().getVigencia() %>" id="PVFCT" type="text" class="form-control" name="<portlet:namespace/>vigencia" >
				</div>
				<div class="col-md-4">
					<label for="fechaSuscripcion"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.fecha-suscripcion"/></label> 
					<input disabled="disabled" value="<%=sdfInputDateValue.format(finding.getAudit().getFecha()) %>" id="fechaSuscripcion" type="text" class="form-control" name="<portlet:namespace/>fechaSuscripcion">
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6">
					<label for="nombre_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.nombre-ejericio"/></label>
					<input disabled="disabled" value="<%=finding.getAudit().getEjercicioNombre() %>" id="nombre_ejercicio" type="text" class="form-control" name="<portlet:namespace/>ejercicioNombre">
				</div>
				<div class="col-md-6">
					<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.tipo-ejericio"/></label> 
					<input disabled="disabled" value="<%=finding.getAudit().getEjercicioTipo() %>" id="tipo_ejercicio" type="text" class="form-control" name="<portlet:namespace/>ejercicioTipo" >
				</div>
			</div>
			<br />
			<h4><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.registro"/></h4>
			<div class="form-row">
				<div class="col-md-3 ">
					<label for="entidad"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.numero-hallazgo"/></label> 
					<input disabled="disabled" value="<%=finding.getCodigo() %>" id="entidad" type="text" class="form-control" name="<portlet:namespace/>numeroHallazgo">
				</div>
				<div class="col-md-3">
					<label for="ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.connotacion-hallazgo"/></label> 
					<input disabled="disabled" value="<%=finding.getConnotacion() %>" id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>connotacionHallazgo">
				</div>
				<div class="col-md-2">
					<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.macroproceso"/></label> 
					<input disabled="disabled" value="<%=finding.getMacroproceso() %>" id="tipo_ejercicio" type="text" class="form-control" name="<portlet:namespace/>macroproceso">
				</div>
				<div class="col-md-3">
					<label for="fecha"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.accion-correctiva"/></label> 
					<input disabled="disabled" value="<%=finding.getAccion() %>" id="fecha" type="text" class="form-control" name="<portlet:namespace/>accionCorrectiva">
				</div>
				<div class="col-md-1">
					<label for="PVFCT"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.meta"/></label>
					<input disabled="disabled" value="<%=finding.getMeta() %>" id="PVFCT" type="text" class="form-control" name="<portlet:namespace/>meta">
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6">
					<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.descripcion"/></label>
					<textarea disabled="disabled" id="descripcion" class="form-control" placeholder="Condicion y causa" rows="2" name="<portlet:namespace/>descripcion"><%=finding.getDescripcion() %></textarea>
				</div>
				<div class="col-md-2">
					<label for="email"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.email"/></label>
					<input disabled="disabled" value="<%=finding.getEmail() %>" id="email" type="email" class="form-control" name="<portlet:namespace/>email">
				</div>
				<div class="col-md-4">
					<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.fecha-terminacion"/></label>
					<input disabled="disabled" value="<%=sdfInputDateValue.format(finding.getTerminacion()) %>" id="tipo_ejercicio" type="date" class="form-control" name="<portlet:namespace/>fechaTerminacion">
				</div>
			</div>
			<br />
			<div class="form-row">
				<div class="col-md-6">
					<label for="direccion">Direccion</label>
					<input value="<%=finding.getDireccion() %>" id="direccion" type="text" class="form-control" name="<portlet:namespace/>direccion">
				</div>
				<div class="col-md-2">
					<label for="cumplimiento">Cumplimiento</label> 
					<input value="<%=finding.getCumplimiento() %>" id="cumplimiento" type="text" class="form-control" name="<portlet:namespace/>cumplimiento">
				</div>
				<div class="col-md-2">
					<label for="efectividad">Efectividad</label> 
					<input value="<%=finding.getEfectividad() %>" id="efectividad" type="text" class="form-control" name="<portlet:namespace/>efectividad">
				</div>
				<div class="col-md-2">
					<label for="estado">Estado</label> 
					<select class="form-select form-control" name='<portlet:namespace/>estado' id="estado">
					  <option value="0" <%=(finding.getEstado().equals("0")) ? "selected=\"selected\"" : "" %>>Abierto-A</option>
					  <option value="1" <%=(finding.getEstado().equals("1")) ? "selected=\"selected\"" : "" %>>Cerrado-C</option>
					</select>
				</div>
			</div>
			<br />
			<div class="form-row">
				<div class="col-md-12">
					<button onclick='return confirm("�Est�s seguro que deseas guardar?")' class="botons" id="crear"><liferay-ui:message key="emcali.hallazgoscontraloria.general.guardar"/></button>
					<a class="btn" href="<%=consultar.toString() %>" ><liferay-ui:message key="emcali.hallazgoscontraloria.general.cancelar"/></a>
				</div>
			</div>
		</form>
	</section>
</div>