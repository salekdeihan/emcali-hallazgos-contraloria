<%@ include file="./init.jsp"%>

<%
	Finding finding = (Finding) renderRequest.getAttribute("finding");
	Audit audit = finding.getAudit();
%>

<liferay-portlet:renderURL var="consultar">
	<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/listadoRegistro" />
</liferay-portlet:renderURL>

<div class="emcali-hallazgos-contraloria">
	<section class="form-register">
		<h2><liferay-ui:message key="emcali.hallazgoscontraloria.registro.listado.mensaje-detalle"/></h2>
		<br />
		<h4><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.entidad"/></h4>
		<div class="form-row">
			<div class="col-md-3">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.nombre-entidad"/></label> 
				<input value="<%=audit.getEntidad()%>" id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>tipoEjercicio" disabled="disabled">
			</div>
			<div class="col-md-3">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.tipo-ejericio"/></label> 
				<input value="<%=audit.getEjercicioTipo()%>" id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>tipoEjercicio" disabled="disabled">
			</div>
			<div class="col-md-3">
				<label for="ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.auditorio-desempeno"/></label> 
				<input value="<%=audit.getAuditoria()%>" id="auditoriaDesempeno" type="text" class="form-control" name="<portlet:namespace/>auditoriaDesempeno" disabled="disabled">
			</div>
			<div class=" col-sm-3">
				<label for="PVFCT"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.pvfct"/></label> 
				<input value="<%=audit.getVigencia()%>" id="PVFCT" type="text" class="form-control" name="<portlet:namespace/>pvfct" disabled="disabled">
			</div>
		</div>
		<br />
		<h4><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.registro"/></h4>
		<div class="form-row">
			<div class=" col-md-3 ">
				<label for="entidad"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.numero-hallazgo"/></label> 
				<input value="<%=finding.getCodigo() %>" id="entidad" type="text" class="form-control" name="<portlet:namespace/>numeroHallazgo" disabled="disabled">
			</div>
			<div class="col-md-3">
				<label for="ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.connotacion-hallazgo"/></label> 
				<input value="<%=finding.getConnotacion() %>" id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>connotacionHallazgo" disabled="disabled">
			</div>
			<div class="col-md-2">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.macroproceso"/></label> 
				<input value="<%=finding.getMacroproceso() %>" id="tipo_ejercicio" type="text" class="form-control" name="<portlet:namespace/>macroproceso" disabled="disabled">
			</div>
			<div class="col-md-3">
				<label for="fecha"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.accion-correctiva"/></label> 
				<input value="<%=finding.getAccion() %>" id="fecha" type="text" class="form-control" name="<portlet:namespace/>accionCorrectiva" disabled="disabled">
			</div>
			<div class="col-md-1">
				<label for="PVFCT"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.meta"/></label>
				<input value="<%=finding.getMeta() %>" id="PVFCT" type="text" class="form-control" name="<portlet:namespace/>meta" disabled="disabled">
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-6">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.descripcion"/></label>
				<textarea id="descripcion" class="form-control" rows="2" name="<portlet:namespace/>descripcion" disabled="disabled"><%=finding.getDescripcion() %></textarea>
			</div>
			<div class="col-md-2">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.fecha-terminacion"/></label>
				<input value="<%=sdfInputDateValue.format(finding.getTerminacion()) %>" id="tipo_ejercicio" type="date" class="form-control" name="<portlet:namespace/>fechaTerminacion" disabled="disabled">
			</div>
			<div class="col-md-4">
				<label for="email"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.email"/></label>
				<input id="email" disabled="disabled" value="<%=finding.getEmail() %>" type="email" class="form-control" name="<portlet:namespace/>email">
			</div>
		</div>
		<br />
		<div class="form-row">
			<div class="col-md-12">
				<a class="botons" href="<%=consultar.toString() %>" ><liferay-ui:message key="emcali.hallazgoscontraloria.general.atras"/></a>
			</div>
		</div>
	</section>
</div>