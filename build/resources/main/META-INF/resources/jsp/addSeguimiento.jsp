<%@ include file="./init.jsp"%>
<%
	Finding finding = (Finding) renderRequest.getAttribute("finding");
	Audit audit = finding.getAudit();
%>

<liferay-portlet:actionURL name="/hallazgoscontraloria/addSeguimiento" var="addSeguimiento" />

<liferay-portlet:renderURL var="listadoSeguimiento">
	<liferay-portlet:param name="mvcRenderCommandName" value="/hallazgoscontraloria/listadoSeguimiento" />
	<liferay-portlet:param name="findingId" value="<%=String.valueOf(finding.getId()) %>"/>
</liferay-portlet:renderURL>

<div class="emcali-hallazgos-contraloria">
	<liferay-ui:error key="error-add" message="emcali.hallazgoscontraloria.seguimiento.add.error-add" />
	<section class="form-register">
		<h2><liferay-ui:message key="emcali.hallazgoscontraloria.seguimiento.add.mensaje"/></h2>
		<br />
		<h4><liferay-ui:message key="emcali.hallazgoscontraloria.fuente.add.detalle-hallazgo"/></h4>
		<div class="form-row">
			<div class="col-md-3">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.nombre-entidad"/></label> 
				<input value="<%=audit.getEntidad()%>" id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>tipoEjercicio" disabled="disabled">
			</div>
			<div class="col-md-3">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.tipo-ejericio"/></label> 
				<input value="<%=audit.getEjercicioTipo()%>" id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>tipoEjercicio" disabled="disabled">
			</div>
			<div class="col-md-3">
				<label for="ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.auditorio-desempeno"/></label> 
				<input value="<%=audit.getAuditoria()%>" id="auditoriaDesempeno" type="text" class="form-control" name="<portlet:namespace/>auditoriaDesempeno" disabled="disabled">
			</div>
			<div class=" col-sm-3">
				<label for="PVFCT"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.pvfct"/></label> 
				<input value="<%=audit.getVigencia()%>" id="PVFCT" type="text" class="form-control" name="<portlet:namespace/>pvfct" disabled="disabled">
			</div>
		</div>
		<div class="form-row">
			<div class=" col-md-3 ">
				<label for="entidad"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.numero-hallazgo"/></label> 
				<input value="<%=finding.getCodigo() %>" id="entidad" type="text" class="form-control" name="<portlet:namespace/>numeroHallazgo" disabled="disabled">
			</div>
			<div class="col-md-3">
				<label for="ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.connotacion-hallazgo"/></label> 
				<input value="<%=finding.getConnotacion() %>" id="ejercicio" type="text" class="form-control" name="<portlet:namespace/>connotacionHallazgo" disabled="disabled">
			</div>
			<div class="col-md-2">
				<label for="tipo_ejercicio"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.macroproceso"/></label> 
				<input value="<%=finding.getMacroproceso() %>" id="tipo_ejercicio" type="text" class="form-control" name="<portlet:namespace/>macroproceso" disabled="disabled">
			</div>
			<div class="col-md-3">
				<label for="fecha"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.accion-correctiva"/></label> 
				<input value="<%=finding.getAccion() %>" id="fecha" type="text" class="form-control" name="<portlet:namespace/>accionCorrectiva" disabled="disabled">
			</div>
			<div class="col-md-1">
				<label for="PVFCT"><liferay-ui:message key="emcali.hallazgoscontraloria.registro.add.meta"/></label>
				<input value="<%=finding.getMeta() %>" id="PVFCT" type="text" class="form-control" name="<portlet:namespace/>meta" disabled="disabled">
			</div>
		</div>
		<br />	
		<h4><liferay-ui:message key="emcali.hallazgoscontraloria.seguimiento.add.detalle-seguimiento"/></h4>
		<form method="post" action="<%=addSeguimiento.toString()%>">
			<input type="hidden" name='<portlet:namespace/>findingId' value="<%=finding.getId() %>">
			<div class="form-row">
				<div class="col-md-2">
					<label for=""><liferay-ui:message key="emcali.hallazgoscontraloria.seguimiento.add.avance"/></label> 
					<input type="number" class="form-control" name="<portlet:namespace/>advance">
				</div>
				<div class="col-md-4">
					<label for=""><liferay-ui:message key="emcali.hallazgoscontraloria.seguimiento.add.fecha-avance"/></label> 
					<input type="date" class="form-control" name="<portlet:namespace/>fecha">
				</div>
				<div class="col-md-3">
					<label for=""><liferay-ui:message key="emcali.hallazgoscontraloria.seguimiento.add.adjunto"/></label> 
					<input type="file" accept="application/pdf" name="<portlet:namespace/>documentoAnexo">
				</div>
			</div>
			<div class="form-row">
				<div class="col-md-6">
					<label for=""><liferay-ui:message key="emcali.hallazgoscontraloria.seguimiento.add.descripcion"/></label>
					<textarea id="descripcion" class="form-control" rows="2" name="<portlet:namespace/>descripcion"></textarea>
				</div>
				<div class="col-md-6">
					<label for=""><liferay-ui:message key="emcali.hallazgoscontraloria.seguimiento.add.observacion"/></label>
					<textarea id="descripcion" class="form-control" rows="2" name="<portlet:namespace/>observacion"></textarea>
				</div>
			</div>
			<br />				
			<div class="form-group col-md-9">
				<button onclick='return confirm("�Est�s seguro que deseas guardar?")' class="botons"><liferay-ui:message key="emcali.hallazgoscontraloria.general.guardar"/></button>
				<a href="<%=listadoSeguimiento.toString()%>" class="btn"><liferay-ui:message key="emcali.hallazgoscontraloria.general.cancelar"/></a>
			</div>
		</form>
	</section>
</div>
